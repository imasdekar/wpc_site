+++
bg_image = "/images/w.jpg"
date = 2022-05-14T12:30:00Z
description = "WPC expresses our sadness for the families of the bereaved and the injured workers in the Mundka fire and also reiterates our solidarity with them. WPC is extremely shocked at the factory fire which engulfed the lives of 30 workers, as per official claims. It is estimated that many more workers who are reported missing, have also perished. **Most of the workers in that building were young women workers.** The massive blaze which engulfed a four-storey building in Delhi’s Mundka on Friday began in a factory on the premises that did not have a No Objection Certificate (NOC) from the fire department. Even worse, the owners did not apply for one. WPC condemns this culpable homicide which occurred due to state negligence and ignorance."
image = "/images/delhi-fire.jpg"
tags = ["india_labour_line"]
title = "Delhi Mundka Factory Fire: A Culpable Homicide Occurred Due to States Negligence and Ignorance "

+++
WPC expresses our sadness for the families of the bereaved and the injured workers in the Mundka fire and also reiterates our solidarity with them. WPC is extremely shocked at the factory fire which engulfed the lives of 30 workers, as per official claims. **It is estimated that many more workers who are reported missing, have also perished. Most of the workers in that building were young women workers.** The massive blaze which engulfed a four-storey building in Delhi’s Mundka on Friday began in a factory on the premises that did not have a No Objection Certificate (NOC) from the fire department. Even worse, the owners did not apply for one. WPC condemns this culpable homicide which occurred due to state negligence and ignorance

The said manufacturing unit which produced high tech electronic and surveillance equipment including CCTV sets operated without any inspection or scrutiny by authorities. This, of course, is not an isolated incident but the tip of the iceberg. The safety of Indian workers is systematically being increasingly jeopardised, as they are forced to make their living working for such firms that have been flourishing and carrying out their production without minimum safety measures.

Of course, the Central Government plans to provide full tacit support for this unsafe working condition environment with the new labour code which will even further systematically remove whatever minimal form of oversight and inspection by the labour departments that currently exists. The Mundka incident is just one more harbinger and such incidents will be the new normal without any impunity. For example, the new standing order which will apply to establishments where over 300 workers are employed/engaged. So, no labour laws related to occupational safety and working conditions will apply to such spaces. As per an estimate, this is over 85% of India’s labour market, especially including those in medium and small enterprises, which often tend to be riskier and more vulnerable to occupational hazards and safety violations. The new wage code also does not cover the large mass of informal workers because its definition of “employer” is ambiguous and narrow. The new wage code says, “Employer means a person who employs, whether directly or through any person, or on his behalf or on behalf of any person”. Many decades of experience have stood witness to the fact that workers struggle to establish their employment relations to get any benefit from the Code provisions. It means legally they don't exist in the labour market.

The Union labour ministry informed Parliament that, at least, 6,500 employees died on duty at factories, ports, mines, and construction sites in the last five years with over 80% of the fatalities reported in factory settings between 2014 and 2018. Factory deaths rose by 20% between 2017 and 2018. 24% of these fatalities are in the construction sector. According to reports by IndustriALL Global Union, between 2014 and 2017, Delhi recorded 1,529 industrial accidents, the highest in the country. There is just one factory inspector for 506 registered factories. We must take note that this number does not include the informal labour market such as the factory in Mundka.

The Working Peoples’ Coalition affirmed their solidarity with the toiling masses and call for halting the notification of all labour laws and demands appropriate amendments keeping in mind India's informal labour market. **We urgently demand to form a high-level tripartite committee with special inclusion of workers' organizations engaging with the informal sector, to review all industrial units that fall under the MSME sector and beyond.** If the government has any intention of acting beyond paying lip service and cosmetic measures, they have to audit safety conditions and subsequently upgrade the infrastructure so that no such incidents happen in the future.

The WPC is forming an independent fact-finding team to thoroughly investigate the matter and make comprehensive representation to state and central govt. We might also initiate litigation proceedings if there are no substantial efforts taken by appropriate authority in the next two weeks.

**Download the Statement here:**

[WPC Statement - Delhi Mundka Fire Incident](https://drive.google.com/file/d/1rGdXlpMMwUmkC8EcY4gZURL7M4YODGN1/view?usp=sharing)

**For more details, please contact:**

Dharmendra Kumar: +91-9871179084

Ramendra Kumar: +91-9871179084

**_On Behalf of WPC Delhi Chapter_**