+++
bg_image = "/images/photo_1-height-500.jpg"
date = 2021-06-02T18:30:00Z
description = "A delegation of Working People’s Charter met the Food and Supplies Minister Chhagan Bhujbal today at Ramtek his official residence to demand implementation of Supreme Court’s order dated 24.5.2021 regarding setting up of the community kitchens by State Governments for migrant workers. A delegation comprising of Adv. Vinod Shetty (ACORN Dharavi Collective), Bilal Khan (GBGBA), Madhuri Jalmulvar (Kashtakari Sangharsh Mahasangh and Shweta Damle (HALWA) gave a list of 18 locations in Mumbai where there is a need to set up community -workers kitchens for migrant workers who are facing distress and loss of livelihood."
image = "/images/photo_1-height-500.jpg"
tags = []
title = "WPC DELEGATION MET MAHARASHTRA FOOD AND SUPPLIES MINISTER SHRI CHHAGAN CHANDRAKANT BHUJBAL TO DEMAND SETTING UP OF COMMUNITY-WORKERS KITCHENS"
type = ""

+++
##### **WPC delegation met Maharashtra Food and Supplies Minister Shri Chhagan Chandrakant Bhujbal to demand setting up of Community-Workers Kitchens**

##### **Minister assured of expansion of Shiv Bhojan thalis to working class neighbourhood**

##### **Press Release**

The unorganised sector has been hit hard due to the ongoing pandemic and lockdown that has resulted in loss of jobs and income in the second wave. This in turn has resulted in food poverty. The meeting was to demand community kitchens as a means of affordable nutritious meals in a nearby location to informal manufacturing hubs and clusters. The need is urgent in anticipation of the 3rd wave and monsoon. The WPC delegation also pointed out to the Honourable Minister that the Shiv Bhojhan thalis announced by the government are not adequate in the current situation and to avoid any hunger deaths the government must urgently increase the number of Shiv Bhojan thalis to more locations with easier access. It was further pointed out that unlike the migrant exodus in 2020 this time around the migrant workers have chosen to stay back in a hope that their employment will resume. But since the lockdown in April 2021 they have not received their wages and are facing financial problems. The delegation asked the government to hasten the distribution of free rations to those without documentation. The delegation further also pointed out the order passed by the supreme court pertaining to the distribution of dry ration and setting up of community kitchens urgently by State Governments apart from the existing welfare scheme under the Disaster Management Act. ShiriBhujbal responded positively to the suggestions made by the WPC delegation and assured them the following-

Shri. Bhujhbal assured the delegation that he is in agreement with the need for community kitchens and shall forward an appeal with his recommendation to Chief Secretary Maharashtra, Mr. Sitaram Kunte.

He further informed the delegation that the Shiv Bhojhan Thalis will be increased as per the demands and needs.

WPC delegation is now planning to meet the Chief Secretary to formalise the recommendations.

Adv Vinod Shetty (98205101460), Bilal Khan (9958660556), Shweta Damle (9833700196)

Please download the representation to hon'ble minister here:

[Letter to Hon'ble Minister of Food and supplies MH](https://drive.google.com/file/d/1P0NDOleFZj-Za-31BX-hHl6LvcYxSyzl/view?usp=sharing)