+++
bg_image = "/images/aurangabadtrain.jpeg"
date = 2020-05-10T18:30:00Z
description = "Statement condemning state negligence leading to the death of 16 workers run over by a goods train in Aurangabad, Maharashtra on the 8th of May, 2020."
image = "/images/aurangabadtrain.jpeg"
tags = []
title = "16 WORKERS RUN OVER BY A GOODS TRAIN, MARTYRED DUE TO STATE NEGLIGENCE IN AURANGABAD, MAHARASHTRA"
type = ""

+++
It is with a deep sense of anguish and distress that we, the Working Peoples’ Charter and Aajeevika Bureau, hereby release this statement to condemn, in the strongest possible terms, negligence on part of the state that has unequivocally lead to the death of 16 workers who were run over by a goods train near Aurangabad. We demand that up a high-level enquiry be immediately initiated to identify those responsible across the offices and departments of the government of Maharashtra and the Indian Railways and that punitive action be taken.

In the early hours of Friday, 8th of May, 16 migrant workers, hailing from Umaria and Shahdol districts in Bihar working in a steel factory in the MIDC zone, Jalna district, who had been walking back home, were killed when a train ran over them reportedly somewhere between the Jalna and Aurangabad districts. They had left Jalna at 7 pm on the previous day, walking on the road for a certain period of time, after which they began walking along the rail track. After walking 36 kilometres, they fell asleep on the track, exhausted. A goods train ran over them the next day.

We would like to point out that this is not an isolated incident. More than 300 people have died, either due to accidents, starvation, exhaustion, suicides and police brutality all of which can be attributed directly to the ongoing lockdown. Invariably, these are deaths that have occurred due to a mismanagement of the crisis

We fail to understand how much more evidence one needs to understand the severity of India’s ongoing migrant crisis. In spite of repeated memorandums, solidarity statements, public appeals, (ongoing) petitions in various courts and continuous coverage in the media, the central and state governments have not done nearly enough to help and protect India’s internal migrants, and specifically in making arrangements for them to get back home. The central and state governments have been constantly changing the guidelines regarding the movement of migrant workers. The central government seems to have devolved all responsibility over transport arrangement on to the state governments, even though the

Ministry of Railways should be playing a key role in this matter. Moreover, there are no clearcut instructions or guidelines on interstate coordination in the last MHA order on ‘movement of people. This too must be the directive of the central government. That migrants in many instances have been asked to ‘pay’ for their train tickets and buses either as a matter of state and/or central government policy or by local cronies through coercion, is nothing short of criminal.

We would like to assert that the Aurangabad incident is in its entirety wired to the mismanagement of the ongoing migrant crisis. Insofar as governments must be made accountable, we firmly demand the following from the government of India:

\- A high-level enquiry be set up to fast track identification of all concerned departments and/or officers in the government of Maharashtra or the Ministry of Railways, whose negligence resulted in the death of the 16 workers. Given the systemic nature of the incident, we passionately believe this cannot be attributed to one government authority or office alone. The issue at hand is not only that migrants were run over by a train, but that their needs were, entirely neglected by concerned public authorities; be it their wages, or sufficient food or accommodation. We further demand that those identified be suspended from public service and that criminal punitive action be initiated against them.

\- The Ministry of Home Affairs should issue a revised order to state governments with more clear-cut directions on ‘how to’ coordinate interstate commute. The existing order is unclear and generic. It only directs states to coordinate commute among themselves. Alternatively, and preferably, the MHA and Railways must work in tandem with state governments to ensure that sufficient resources are available to move migrants back to their villages.

\- Migrant workers continue to walk back home despite the arrangements made for trains. There is an urgent need to introduce comprehensive support mechanisms at regular intervals, including provision of water, food and sanitation facilities are made available to those walking such that no more migrants die. We demand that migrants found following rail tracks to get home be provided with immediate assistance at regular intervals, irrespective of domicile or duration of their commute.

This must include putting them onto the earliest and nearest available special train possible.

\- We further demand that the ongoing onslaught of charging migrants for their commute home be stopped entirely and all over the country. Anyone seen charging migrants for the same should be arrested immediately.

The government has a responsibility towards protecting its most vulnerable and marginalized citizens in these difficult times. The ongoing migrant crisis is a matter of national shame. It is indicative of a faltering welfare state. The Aurangabad incident is the fiercest indication in this direction thus far. It is therefore all the more important that the government acts upon our demands swiftly and diligently.

Working Peoples’ Charter

Aajeevika Bureau

For more information please contact :

Chandan Kumar - wpcindia@protonmail.com

Divya Varma- divya.varma@aajeevika.org

M- +91 9717891696 , 9606359333

[**Click here to read the full Statement**](https://drive.google.com/file/d/1lEqYeAGg_H-cR2M77MjP7jyY9-flUy8T/view?usp=sharing)