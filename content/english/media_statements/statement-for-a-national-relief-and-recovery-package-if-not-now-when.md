+++
bg_image = "/images/pic_3-height-500.jpg"
date = 2021-06-13T18:30:00Z
description = "The impact of the two COVID waves and attendant lockdowns on Indian households and workers cannot be overstated. Even before the devastating second wave, several sources suggest that millions of households became poor, reversing hard-fought gains in poverty reduction. More than half of the informal workforce lost work and incomes, and over two-thirds experienced increased hunger (see Annexure 2 in this note). Poorer households have borne the brunt of the impact and inequality has, without doubt, worsened and deepened. The ongoing second wave and localised restrictions have not only exacerbated the shocks of 2020 for the urban poor and migrant workers but have also made rural households and non-poor households newly and deeply vulnerable."
image = "/images/pic_3-height-500.jpg"
tags = []
title = "STATEMENT FOR A NATIONAL RELIEF AND RECOVERY PACKAGE: IF NOT NOW, WHEN?"
type = ""

+++
##### **STATEMENT FOR A NATIONAL RELIEF AND RECOVERY PACKAGE: If Not Now, When?**

The impact of the two COVID waves and attendant lockdowns on Indian households and workers cannot be overstated. Even before the devastating second wave, several sources suggest that millions of households became poor, reversing hard-fought gains in poverty reduction. More than half of the informal workforce lost work and incomes, and over two-thirds experienced increased hunger (see Annexure 2 in this note). Poorer households have borne the brunt of the impact and i**nequality has, without doubt, worsened and deepened.** The ongoing second wave and localised restrictions have not only exacerbated the shocks of 2020 for the urban poor and migrant workers but have also **made rural households and non-poor households newly and deeply vulnerable**. Women, children, and socially disadvantaged groups, in particular, are at a higher risk of falling behind over the longer term. Unless bold steps are taken, these effects may be long-lasting.

Even prior to the pandemic, the Indian economy had experienced several quarters of economic slowdown resulting in high unemployment and wage stagnation. The income shock of the pandemic has further depressed aggregate demand. Beyond the necessary focus on vaccination and health systems, therefore, a rapid macroeconomic recovery requires an urgent response in the form of a **National Relief and Recovery Package** to: (a) protect life, (b) partially compensate for lost livelihoods and income, and (c) to boost demand in the economy for faster overall recovery. Without the **direct support** of such a Package, simply unlocking the economy will not lead to a balanced recovery. Advanced and developing economies across the world are investing in similar state-led recovery programmes that seek to boost household income and spending, recognizing the need for large scale relief and recovery interventions into the economy. India must do the same.

This statement focuses on three minimum and necessary elements of this package: **food, income, and work.** The near-universal impact of the second wave means that we focus on a larger set of vulnerable households beyond, for e.g., just those included under the National Food Security Act (NFSA). The Package must thus cover **27cr households** in all which is about **82% of all households** in the country (see Annexure 1 in this note). Building on and expanding the 2020 national relief package, we detail key components in the table below while also suggesting additional measures on loans and credit in Annexure 1.

**Food**

The extension of expanded food rations to PDS cardholders till November 2021 is welcome. We should further leverage the 100mn tonnes of food grain (over three times the buffer stock norms) for:

* Expanding food distribution to non-PDS cardholders till November 2021 to reach vulnerable households outside the PDS system.
* Specific expansions for families with children to ICDS delivery, and additions to rations as well as meals at schools (including eggs) and Anganwadi

**Income**

* Undertaking crisis cash transfers of Rs 3000 per month for six months

**Work**

* Expanding NREGA work entitlements to 150 days
* Initiating immediate public works programmes for urban employment

Clear delivery mechanisms exist with precedence for recommendations on Food and Work, with known and accepted fiscal allocations. For Income, the proposed crisis cash transfer must leverage **existing direct benefit transfer systems** (NREGA, PM-KISAN, PMJDY, NSAP) with new **decentralized systems of direct** distribution from ration shops, post offices, panchayats and other local institutions. We anticipate, as detailed in Annexure 1 in this note, that the proposed income transfer will cost the Government of India an additional **Rs 4.44 lakh crore, or 1.97% of the projected 2021-22 GDP**. The Centre must lead in this package, with minimal cost-sharing with states who focus on delivery and use their own funds to expand the reach of the package, particularly in urban areas.

It is essential that the Government of India recognize the need for directed, equitable and dignified economic recovery for India’s workers and citizens. We urge it to act urgently, following its constitutional obligations as well as global best practice. If not now when?

**Endorsements:**

* Aajeevika Bureau
* All India Central Council of Trade Unions (AICCTU)
* Andhra Pradesh Domestic Workers Federation
* Andhra Pradesh Vyavsaya Vruthidarula Union (APVVU)
* Asangathit Kaamgaar Adhikar Manch
* Assam Mazdoor Union
* Association of Rural Education and Development Service (AREDS)
* Bandhua Mukti Morcha
* Bhartiya Kamgaar Sena
* Centre for Amenities, Rehabilitation and Education (CARE)
* Centre for Financial Accountability
* Centre for Labor Research and Action
* Chetna Andolan, Uttarakhand
* CORD, India
* Dagadkhan mazdoor kalyan parishad
* Dakshinbanga Matsyajibi Forum (DMF)
* Dalit Bahujan Resource Centre
* Dalit Media Watch
* Delhi Shramik Sangathan
* FIRA- Federation of Indian Rationalist Association
* Ghar Bachao Ghar Banao Andolan
* Gram Vaani
* Grameena Koolikaarmikara Sanghatane (GRAKOOS)
* Habitat and Livelihood Welfare Association
* Hamal Panchayat
* HASIRUDALA
* Hawkers Joint Action Committee
* Indian Federation of App based Transport workers (IFAT)
* Indian Labour Union (ILU)
* International Federation of Trade Unions (IFTU)
* Jan Chetna Sansthan
* Jan Jagriti Shakti Sangathan
* JSA-MUMBAI
* Kaamgaar Ekta Union
* LibTech India
* Maharashtra Kashtakari Sangharsh Mahasangh
* Mahila Kisan Adhikar Manch (MAKAAM)
* MANS- Maha ANiS- Maharashtra Andhashraddha Nirmulan Samity.
* Mazdoor Kisan Shakti Sangathan
* Meghalaya & Greater Shillong Progressive Hawkers & Street Vendors Association
* Nari Atyachar Virodhi Manch (Forum Against Oppression of Women), Mumbai
* National Alliance of People's Movements (NAPM)
* National Campaign Committee on Central legislation of construction workers
* National Centre for Advocacy Studies
* National Centre for Labour
* National Confederation of Dalit and Adivasi Organisations (NACDAOR)
* National Fish Workers Forum
* National Hawkers Federation
* National Platform for Small Scale Fish Workers (NPSSFW)
* National Workers Movement
* PAIGAM Network (People's Association in Grassroots Action and Movements) and Akriti Bhatia
* Partnering Hope into Action Foundation (PHIA)
* Prayas Centre for Labor Research and Action
* RAIOT Collective, Meghalaya
* Rajasthan Gharelu Mahila Kaamgaar Union
* Recyclers association
* Right to Food Campaign India
* Rural Uplift Centre
* Rythu Swarajya Vedika
* Sarvahara Jan Andolan
* Shramik Adhikar Manch
* Social Accountability Forum for Action and Research (SAFAR)
* Telangana Domestic workers union
* Thma U Rangli-Juki (TUR)
* Tirupur Peoples Forum
* Trade Union Centre of India (TUCI)
* United Nurses Association
* Vaan Muhill
* Workers Power of Meghalaya, Meghalaya
* Youth for Unity and Voluntary Action (YUVA)
* YUGANTAR

### **Please download the full statement and annexures here:**

[STATEMENT FOR A NATIONAL RELIEF AND RECOVERY PACKAGE](https://drive.google.com/file/d/1_kQ7Q_jzac0w21Mspn3WiywRJx-3d1gN/view?usp=sharing)