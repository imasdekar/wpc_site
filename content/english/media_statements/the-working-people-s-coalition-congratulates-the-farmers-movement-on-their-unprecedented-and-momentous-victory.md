+++
bg_image = "/images/154475-ihmxlfavva-1612625479.jpeg"
date = 2021-12-10T18:30:00Z
description = ""
image = "/images/154475-ihmxlfavva-1612625479.jpeg"
tags = []
title = "The Working People’s Coalition congratulates the Farmers’ Movement on their unprecedented and momentous victory"
type = ""

+++
**PRESS RELEASE**

**11th December 2021**

#### **_The Working People’s Coalition congratulates the Farmers’ Movement on their unprecedented and momentous victory_**

The Samyukt Kisan Morcha (SKM) has successfully concluded its historic year-long farmer’s struggle at the borders of Delhi. The Working People’s Coalition (WPC) would like to wholeheartedly congratulate the leaders, unions and groups who undertook this year-long struggle.

The WPC, which stands for the collective voice of all of India’s informal and unorganized sector workers, has found inspiration in this model of resistance that the farmer’s movement has instituted. The mammoth participation of women farmers has also revitalized the diverse WPC constituencies from all corners of India who had continued to show solidarity and offer support to the movement.

The 715 martyred farmers will be remembered by the present and all future generations as heroes who refused to surrender to the State-led neo-liberal corporate raj. The WPC salutes the bereaved families for their strength and solidarity with this movement.

The WPC will keep watch along with the SKM as they continue to monitor and ensure that the government fulfils the promises that it has made. The SKM has clarified that the struggle has not ended, but has merely been suspended.

Indian agriculture is in deep crisis and needs urgent and sustainable reforms. There are thousands of farmers who still earn their livelihood through cultivation and hence have to be involved in consultations which impact them.

It is only through the unity between workers and farmers that the lives of India’s working class can be materially improved. In search of this new dawn, the WPC resolves to continue to struggle, agitate and support all peoples’ movements and stand with workers when their interests are under attack.

**In Solidarity,**

**Working Peoples’ Coalition**

**For more details contact: +91 97178 9169, 9920486727**

[Working Peoples' Coalition - Press Note - 11.12.21 - English](https://drive.google.com/file/d/1vZaV5saPBP8QBvGaZTt6Lvp83GacdiZh/view?usp=sharing)