---
title: WPC Media Statements
bg_image: "/images/1129018.jpeg"
description: "**Our responses to recent developments in labour policy**"
menu:
  main:
    name: Media statements
    URL: media_statements
    weight: 10

---
