+++
bg_image = "/images/launch_1-height-500.jpg"
date = 2021-08-05T18:30:00Z
description = "At the launch of the report on ‘Workers Housing Needs and the Affordable Rental Housing Complexes (ARHC) Scheme’, authored by Mukta Naik, Swastik Harish and Shweta Damle, published by WPC, CPR, and India Housing Report, held on 6th of August 2021, at the Constitution Club, New Delhi, unanimously passed a resolution on the critical issue of rental housing for workers. The report is an attempt to make sure that the area of workers rental is properly thought out and implemented and solutions are found."
image = "/images/launch_1-height-500.jpg"
tags = []
title = "LAUNCHING OF THE REPORT ‘WORKERS HOUSING NEEDS AND THE AFFORDABLE RENTAL HOUSING COMPLEXES SCHEME (ARHC)'"
type = ""

+++
At the launch of the report on ‘Workers Housing Needs and the Affordable Rental Housing Complexes (ARHC) Scheme’, authored by Mukta Naik, Swastik Harish and Shweta Damle, published by WPC, CPR, and India Housing Report, held on 6th of August 2021, at the Constitution Club, New Delhi, unanimously passed a resolution on the critical issue of rental housing for workers. The report is an attempt to make sure that the area of workers rental is properly thought out and implemented and solutions are found.

The ARHC scheme that was announced by the Government of India as a part of the Rs 20 lakh crore Atmanirbhar Bharat Abhiyan relief package, envisages the creation of affordable rental housing for the migrants and the urban poor. The mode of delivery is based on involving private and public agencies for the upgradation and operation of the existing vacant housing units that were constructed in the past under other government schemes. It also encourages private and public institutions to construct workers housing on their own land through a concessionaire agreement and providing a viability gap fund.

Mukta Naik from Centre for Policy Research who is the author of the report explained that ARHC scheme needs to be more nuanced as it is articulated for a broader target group, it is the most vulnerable including Muslims and SC/ST households who are getting excluded.

Underlining that housing needs to be understood as a social upliftment vehicle and rental housing needs to incorporate this understanding, she remarked that “this is the first-time rental housing has been paid some attention in policy. But does such a scheme benefit the constituency it was aimed at? While the focus on rental housing for workers is a positive step, the findings from the report indicate several gaps in the policy.”

Elaborating on the findings of the study she highlighted that the “the concerns are in terms of quantity of housing that clearly indicate gross inadequacy. The quality of housing under the scheme, was found to be extremely substandard. The civic amenities available at the site are scarce or non-existent. These housing projects are located far from the location of work, making them unviable options for urban poor and migrant workers. The profit-oriented nature of the scheme implies that these rental projects will largely cater to salaried and formal workers, rather than the urban poor and migrant workers in whose name it has been formulated. The scheme is at best a very partial solution to address the issue of migration and housing in cities.”

Meena Menon from the Working Peoples’ Charter added by stating that, “When this scheme was announced, Working Peoples’ Charter in its different state chapters assisted in the research and data collection. As migrant workers are creating wealth, and it is the state’s responsibility to provide them with housing and jobs that are near rental houses. There have been some improvement as large-scale slum demolition cases have receded due to multiple policies, but a lot more needs to be done. Information is the first step for any struggle. And this report is the start of an agitation that we must lead with different public-interest organizations.”

Gautam Bhan from Indian Institute of Human Settlements further commented stating that “movements, NGOs and researchers must work collectively to carry out original research. This report has been a product of such collective collaboration and has disseminated crucial information for social movements fighting for affordable housing. The fight against evictions must start 10 years before the bulldozers come. The issue of rental housing is our reality, and a lot of working-class citizens need this rental housing. It is important to include this issue in our broader demands.”

Manoj Jha from the Rashtriya Janta Dal made crucial contributions stating that “no one wants to leave their home. 1989 was a painful year for me when I left my home for Delhi. We are asking for answers from people who do not want to give answers. They want the sub-altern to be kept at the boundaries. I do not oppose markets, but we cannot be at the mercy of markets. There should be a greater solidarity in civil society groups in order to increase collaborative projects with the state. We have to teach the political class to be better.”

Senior Activist Indu Prakash highlighted that during the period of evictions, the people would say we have been here since the past thirty years. He stated that “it is not that flats are not being made, it is much more complex than just a location, just a flat. Policies are not made to address why one should not transfer from one city to another. It is about the individual and their challenges that need to be addressed within their location, and the approach to such challenges require analysing and identifying the challenges within the space of that individual and not just by creating a shift.”

Bilal Khan from Ghar Bachao Ghar Banao Andolan made observations saying, “Housing rights movements and organizations do not solely focus on rental housing and through this report this issue has been highlighted. Today with the introduction of this scheme the question that crops up is to what extent are the struggles being addressed with this scheme? The report focuses on the seasonal migrants and their issues, is there a way to make their issues more inclusive?”

Focussing on the need to create an inclusive movement for sustainable and affordable housing, Working People’s Charter campaigner, Chandan Kumar stated “In today’s time, it is really important, not to just map the specific locations and what requirements are there in those specific locations but also to direct ourselves and our understanding of this new regulation and the purpose of it as well as the challenges that come with it. Slums need to be treated with utmost care and in conditions that are suitable to the living needs.”

Nirmal Gorana, General Secretary of Bandhua Mukti Morcha in closing said that “there are 50 crore working class citizens in India. These are the people who have created this country. Most of these are not even recognized as labour. 1,00,000+ working class citizens were evicted during a pandemic by this government in Khori Gaon at the borders of Delhi. A voice of the invisible needs to be raised and the sufferings need to be compensated for. This report addresses the complex system that calls for a critical analysis to understand the voices of these people and the fight they are putting up with.”

The participants called upon labor and civil society organizations to take forward the issue of affordable, adequate and dignified workers housing in general and affordable rental housing in particular as a critical demand of millions of informal sector workers living in the different cities in India, who have no security of shelter, no means to afford decent housing, no security from arbitrary eviction, no access to decent civic and other public services.

**Meena Menon and Chandan Kumar**

**Working Peoples’ Charter (WPC) Network**

##### Please download the full statement, ARHC report and Resolution here:

[Statement - ARHC report launch](https://drive.google.com/file/d/14v4nLwfC9oeBiaKicbnHkJ7sAy-PJ4in/view?usp=sharing)

[ARHC Scheme report - 30072021](https://workingpeoplescharter.in/publications/workers-housing-needs-and-the-affordable-rental-housing-complexes-arhc-scheme/)

[Resolution - ARHC Scheme report launch](https://drive.google.com/file/d/1E4MSfYGv_5eG8M6A3OQ5zdI-M5lja2Gt/view?usp=sharing)