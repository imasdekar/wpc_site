+++
bg_image = "/images/w.jpg"
date = 2021-09-10T18:30:00Z
description = "The Working Peoples’ Charter – Maharashtra chapter held a state-wide wide meeting on 11th September 2021 which saw enthusiastic participation of numerous unions and workers’ organizations from Vidharbha, Sangli, Satara among others. In remembrance of housing rights activist Datta Iswalkar and feminist Sonal Shukla, the attendees expressed solidarity."
image = "/images/wpc_nc-height-500.jpg"
tags = []
title = "WPC HOLDS MAHARASHTRA-WIDE MEETING - RAGPICKERS, TRANS, SEX WORKERS AND FISHERFOLK PARTICIPATE"
type = ""

+++
The Working Peoples’ Charter – Maharashtra chapter held a state-wide wide meeting on 11th September 2021 which saw e**nthusiastic participation of numerous unions and workers’ organizations from Vidharbha, Sangli, Satara among others.** **In remembrance of housing rights activist Datta Iswalkar and feminist Sonal Shukla, the attendees expressed solidarity.**

Starting on a bleak note, Brian voiced a significant concern and commented, “We are not coming together as a major political force and the reasons for this must be discussed. We need a stronger coalition to address challenges from different sectors.”

Encouraged by his initiation, members of Maharasthra Rajya Samnavay Kamgar explained, “the effect of the lockdown has raised many challenges which have been multiplied by the inefficient and disconnected functioning of local and national unions. To reduce this distance created with workers, we must come together with the WPC.”

Stressing on the need to unionize Vinod in the presence of senior activist Ulka Tai echoed, “Governments will only listen if we possess the strength to make them listen. Even formal union workers are being pushed towards the unorganized sector through contractualisation.”

**The worst hit during the pandemic-induced lockdowns has been sex workers who have had to stop their work to keep society safe.** “Our work isn’t even considered work and despite many institutions including the Supreme Court promising support – we have received nothing and have been abandoned. We need to create stronger unions to protect our interests.”

Sparked by such a discussion, women workers particularly domestic workers’ plight was also underlined. The lack of any seriousness shown by the police doubles down on the apathy shown by the governments who have refused to provide any relief in such disastrous times.

A significant question was raised about the exclusion of trans voices. Since their gender was still not recognized, **the first step that the movement should focus on was to get them their desired identity and dignity. Since they did not have IDs** – they needed recognition of their work so that they would be able to gain access to employment and education opportunities.

**This discussion led to a focus on the E-Shram portal –** which even though provides for trans workers – in its process forces them to prove their identity which is deeply dehumanizing. Moreover, the portal is also being rendered inaccessible due to it being online, restricted to workers with a phone-linked Aadhar. **Kachra Vahtuk Shramik Sangathan and Ghar Bachao Ghar Banao Andolan took up the responsibility to train members to set up centres to simplify registration woes.**

Outlining the impacts of climate change on marginalized workers, Kiran Koli from the Maharashtra Machhimar Samiti said, “Maharashtra has been facing numerous cyclones. But the government’s relief schemes are usurped by boat owners, and nothing is trickled down to workers like fish dryers, sellers, cold storage workers etc.”

Meena Menon the national head for WPC stated that “the informal sector is big, and many formal industries are also at times covered under this. But it is not sufficient to just come together. **We need to think of newer ways to gain mileage for political bargaining. We need to create new demands and always keep the interests of workers and the unemployed in mind.**”

Speaking on the need to gather political and legal support in the legal fraternity, Senior Adv. Gayatri Singh remarked that “We need to create a network of lawyers who are committed to fighting for workers’ issues. Legal literacy holds key importance, and we must make presentations in law colleges, among worker’s groups and collectives, with activists to expand the network and ensure implementation of established labour regulations.”

Chandan Kumar, the national coordinator for WPC concluded the session with his insightful comments stating that “**the formal and informal sector workers must come together to bridge the divide caused in ESIC and EPFO policies. The law of unemployment and health compensation must be expanded in terms of policy and infrastructure, and we must join hands with movements like the Jan Andolan to make health schemes portable and accessible.**”

An agenda focusing on a campaign for ensuring maximum participation for the E-Shram worker registration process was advanced. This would also advance the pursuit for a social security policy that includes schemes targeting housing, income, and universal social security. In order to pursue this, newer organizations working across Maharashtra were to be identified and then be united for this struggle.

**WPC Maharashtra Chapter Coordination Team**

Bilal Khan, Gayatri Singh, Kashinath Nakhate, Raju Bhise, Raju Vanjare and Shweta Tambe