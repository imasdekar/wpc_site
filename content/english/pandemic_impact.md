---
title: Pandemic and the impact on informal labour
bg_image: "/images/w.jpg"
description: Although the Government of India and various state governments have made
  policies and interventions to help stranded migrants, however a majority of workers
  are unaware of government programmes or have not been reached by them.
type: theme
facts:
  enable: false
  fact_item:
  - name: Workers Supported
    count: 60
  - name: Meals Provided
    count: 50
  - name: Funds Raised
    count: 1000
  - name: Workers Vaccinated
    count: 1400
success_story:
  enable: true
  bg_image: images/backgrounds/home_video.jpg
  title: OUR EFFORTS ON GROUND
  content: The COVID-19 pandemic has given us the opportunity to finish the unfinished
    agenda of entitlements and advancement of the informal workers. Portable identity
    across state borders along with universal social protection including living wages,
    pension, maternity, health entitlements, and unemployment allowance are urgent
    needs of the hour. In the absence of this, the current state of informal workers
    in India looks bleak. Absence of urgent economic support will only further their
    already existing vulnerabilities, marginalization, and exploitation. They will
    continue to be treated as disposable and stateless persons.
  video_link: https://www.youtube.com/watch?v=hIq-FXXadE0
menu:
  main:
    name: Pandemic Impact
    URL: pandemic_impact
    weight: 6
    parent: home

---
{{< youtube fHFy-t0tFoY >}}

***

#### [Statement of Working Peoples' Charter ](https://drive.google.com/file/d/1Gq9ZcF1nhySPOacwGkvPyJmGlCTeA2EY/view?usp=sharing)

The ILO reports that the COVID-19 crisis would push around 40 crore workers in India's informal sector - comprising a bulk of the country's economy — deeper into poverty, including ITS migrant workers.

***

##### Media Coverage

The Working Peoples' Charter has issued a statement addressing the coronavirus crisis in India, and the workers affected by the lockdown.
**Measures must protect our people, in particular the most vulnerable -
including the elderly, the sick and the poor.**

***

##### Policy Intervention

Although the Government of India and various state governments have made policies and interventions to help stranded migrants, a majority of workers are unaware of government programmes or have not been reached by them. It is imperative given this situation that both government policies are massively and rapidly strengthened in this regard, even as communities come forward to help stranded migrants in need!

[Letter to Ms Nirmla Sitharaman, Chairperson Covid-19 Economic Task Force, GOI](https://drive.google.com/file/d/1Gq9ZcF1nhySPOacwGkvPyJmGlCTeA2EY/view?usp=sharing)

***

##### [Statement of Working Peoples' Charter on Coronavirus crisis in India](https://www.thehindu.com/news/national/statement-of-working-peoples-charter-on-coronavirus-crisis-in-india/article31256395.ece)