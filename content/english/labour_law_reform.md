---
title: Labour Law Reform
description: Solidarity with Indian trade unions demanding the repeal of anti-worker
  provisions of the new labour codes
bg_image: "/images/w.jpg"
type: theme
facts:
  enable: false
  fact_item:
  - name: Workers Supported
    count: 60
  - name: Meals Provided
    count: 50
  - name: Funds Raised
    count: 1000
  - name: Workers Vaccinated
    count: 1400
success_story:
  enable: false
  bg_image: ''
  title: ''
  content: ''
  video_link: ''
menu:
  main:
    URL: labour_law_reform
    weight: 4
    parent: home

---
##### The government of India is aware of the widespread discontent among workers’ organizations with their approach to drafting and legislating the four Labour Codes. The WPC shares the concerns raised by the trade unions and other stakeholders. In this, the singular failure of the government in sustaining the democratic social dialogue process and respecting historic tripartite institutions such as the Indian Labour Conference stands out.

1. We demand that the government restore and strengthen democratic consultation processes on labour law reforms, and labour administration in general, to align all new laws with the concerns of the working people of India. **We stand in solidarity with Indian trade unions demanding the repeal of anti-worker provisions of the new labour codes.** We note with concern the continuing informalisation of much of what was once a formal sector, the restrictions on unionising, and the lack of access to the justice system. We insist that the government guarantee the protection and enlargement of labour rights under the new labour codes with time-bound progress towards formalization of the unorganized sector. The restriction on the number of workers should not be a criterion for access to entitlements.
2. In echoing the people’s voices on ongoing legislative reforms in the country, the WPC also reiterates its solidarity with the ongoing movement of the farmers against the farm laws.