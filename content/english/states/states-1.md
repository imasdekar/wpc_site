---
title: 'Tamil Nadu '
date: 2020-03-09T15:27:17+06:00
bg_image: "/images/media_statements/statemet_on_labour.jpg"
description: ''
image: "/images/media_statements/statemet_on_labour.jpg"
location: Chennai, Pondicherry
state_website_url: "#"
members:
- name: Jack Mastio
  image: images/event-speakers/speaker-1.jpg
  designation: Teacher
- name: John Doe
  image: images/event-speakers/speaker-2.jpg
  designation: Teacher
- name: Randy Luis
  image: images/event-speakers/speaker-3.jpg
  designation: Teacher
- name: Alfred Jin
  image: images/event-speakers/speaker-4.jpg
  designation: Teacher

---
* Enlarged the number of organizations and extended to Pondicherry.


* Immediate demand – protecting informal workers – ensure cash transfers and food.


* Ensuring vaccination to everyone.


* We need to make sure that especially the panchayat can see who is affected and then implement social protection by the state.


* We are finagling all the demands by 3rd June – and hand over the demand to the CM.


* Launching of the English version of the report with the finance minister and other ministers – 9th or 10th of June (not later than 15th June depends on the availability of ministers)


* Pondicherry will take lead from the Tamil Nadu experience to form WPC

   
* Pondicherry by bringing together all the trade unions and workers collective. A similar charter of demands will be handed over to the CM of Pondicherry after TN.