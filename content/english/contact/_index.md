---
title: Contact Us
bg_image: "/images/w.jpg"
description: 'The Working Peoples'' Coalition is looking for volunteers and students
  with a passion for selfless service to help improve the lives of millions of working-class
  Indians. '
menu:
  main:
    name: Contact
    URL: contact
    weight: 15

---
**India Labourline is open to accepting applications from law students for a 2-month internship.**

As interns at India Labourline, your work will give you direct exposure to issues faced by the informal sector workers in India. It will help you develop a keen understanding of various stakeholders at play in ensuring the implementation and protection of the fundamental and labour rights of some of the most vulnerable classes of people in India. You will have a hands-on experience of Labour laws and their use in a practical real-world setting. You will also be providing legal inputs to the tele-counsellors who advise the workers in the first instance.

Selected applicants will be expected to maintain the highest ethical standards and help us progress to better serve the needs of the workers in need of our assistance.

**We are looking for interns who will participate in:**

1. Research work and writing: Carry out research on various labour laws and precedents on specific issues.
2. Help draft simplified legal literacy material of various labour laws for training and outreach.
3. Fieldwork: Accompany the field team during outreach and legal- aid work. Details:

**Duration:** 2 months

**Location:** Mumbai

**Eligibility:** We are looking for students who have already studied Labour laws in the course of their law programs. Students from the 4th and 5th year enrolled in recognised 5-year law programs; Students from the 2nd and 3rd year enrolled in recognised 3-year law programs and students from recognised LL.M. programs at any Indian university.

**How to Apply?**

We will accept applications for this round until 30 June 2022.

To apply, **please email as an attachment your CV along with a brief cover letter (300 words) explaining why you are interested in this internship**.

**Email your applications to:** legal@indialabourline.org