+++
bg_image = "/images/screenshot-2022-01-19-at-5-07-44-pm.png"
date = 2020-08-31T04:09:41Z
description = "This report authored by Rahul Menon and Chinmayi Naik examines whether there have been changes in the decision of migrant workers to return to cities in order to find work. This report finds that a significant share of workers report not wanting to return to cities; moreover, a high proportion of workers who do wish to return reveal that they would only return once the pandemic has been brought under control."
image = "/images/screenshot-2022-01-19-at-5-07-44-pm.png"
source_url = "https://drive.google.com/file/d/1Fwu2dPm91pHs_bCYnDYhK2j4h0yukJHX/view?usp=sharing"
tags = ["labour_axis", "Publications"]
title = "After the Long Marches: What do Workers want? "
type = ""
[[publishers]]
designation = ""
image = ""
name = "Rahul Menon"
[[publishers]]
designation = "Research Coordinator, Working Peoples' Charter "
image = ""
name = "Chinmayi Naik"

+++
Working People's Charter- **a national alliance of informal sector workers (WPC)** presents a preliminary survey titled “After the long marches: What do workers want?” surveying migrant workers – who returned home from big cities due to the lockdown - across some of the major states in India.

This survey examines whether there have been changes in the decision of migrant workers to return to cities in order to find work. This report finds that a significant share of workers report not wanting to return to cities; moreover, a high proportion of workers who do wish to return reveal that they would only return once the pandemic has been brought under control. This indicates that the goals of control of the pandemic and economic expansion must not be thought of as being mutually exclusive.

The report is authored by **Rahul Menon and Chinmayi Naik.**

This report presents a survey of 132 workers across some of the major states in India, states that account for a significant majority of migrant’s labour in the country. Respondents consist of migrant labour who had returned home from the big cities as a result of the lockdown. This survey aimed to examine whether there have been changes in the decision of migrant workers to return to cities in order to find work. One of the **major findings of this report is the fact that close to 30% of workers in our sample have expressed a desire not to return to cities to find work and would take up NREGA work or any other sort of work instead of migrating back. It highlights the dangers in assuming that things would return to normal following the pandemic and when economic activity is allowed to resume. Even though a majority of workers in our sample have expressed a desire to return, a 30% reduction in labour supply – if it were to occur – would serve as a significant constraint on the ability of the economy to expand.** Moreover, even amongst those workers who wish to return, their decision to return is conditional on the virus being controlled, and the chances of infection reducing. All things considered, this survey indicates that in the near future, a policy cannot assume that migrant workers would return to the cities in the same numbers as before. There is no distinction between a robust policy of public health, and one aimed at economic expansion.

This report is well aware of the danger in extrapolating this result to the entire country. For one, the sample size is relatively small – only 132 workers were interviewed. Moreover, as the weeks and months go by, and livelihood opportunities may dry up in workers’ hometowns, all those workers who said that they would not return to the city might rethink their decision and migrate due to necessity. Hence, with support, modifications, and improvements the research can be enhanced.

[**DOWNLOAD THE REPORT HERE**](https://drive.google.com/file/d/1Fwu2dPm91pHs_bCYnDYhK2j4h0yukJHX/view?usp=sharing)